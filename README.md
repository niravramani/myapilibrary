# MyAPILibrary

A description of this package.

//In AppDelegate 
    
    import MyAPILibrary 

// in AppDelegate's  didFinishLaunchingWithOptions set network environment
    
    APIManager.networkEnviroment = .dev
    
// to set number of retries for api call  ( it will reset to 0 after every api call so make sure you set it  up for apis which requires multiple attempts to retrive data )   --> Need to call just before API call

    apiManager.setNumberOfRetries(number : 1) // where apiManager is a object of APIManager

//  Create a file named "RequestItemsType.swift" and do copy paste below code ( code with pink color)

    import Foundation
    import MyAPILibrary

    public enum RequestItemsType {
        case getDataSourceLink
    }

    //MARK: - Extension 
    extension RequestItemsType: EndPointType {
    
        // MARK: - Vars & Lets
    
        public var baseURL: String {
            switch APIManager.networkEnviroment {
                case .dev: return "Write your development api link here"
                case .production: return "Write your production api link here"
                case .stage: return ""
            }
        }
    
        public var version: String {
            return "/v0_1"
        }
    
        public var path: String {
            switch self {
            case .getDataSourceLink:
                return "GetDataSourceLink"//API end point name will be return from here
            }
        }
        // if your api is having httpmethod type get then return .get else .post
        public var httpMethod: HTTPMethod {
            switch self {
                case .getDataSourceLink:
                    return .post
                default:
                    return .post
            }
        }
    
        public var headers: HTTPHeaders? {
            switch self {
            default:
                return ["Content-Type": "application/json",
                    "X-Requested-With": "XMLHttpRequest"]
            }
        }
    
        public var url: URL {
            switch self {
            default:
                return URL(string: self.baseURL + self.path)!
            }
        }
    
        public var encoding: ParameterEncoding {
            switch self {
                default:
                return JSONEncoding.default
            }
        }
    }

// end of file :- RequestItemsType.swift

// Here is an example that how to call an api

    // create a variable of APIManager Class
    private let apiManager = APIManager(sessionManager: SessionManager(), retrier: APIManagerRetrier())

    // sample function for api calling s
    func getDataSourceLink(){
       let param: Parameters = [
           "device_type": "0",
           "device_token": ""
       ]
       self.apiManager.call(type: RequestItemsType.getDataSourceLink, params: param) { (res: Swift.Result<BaseClass<[DataSourceLinkClass]>, AlertMessage>) in
           switch res {
           case .success(let data):
               for dataSource in data.data ?? []{
                   print(dataSource.link ?? "")
               }
               break
           case .failure(let message):
               print(message)
               break
           }
       }
    }
