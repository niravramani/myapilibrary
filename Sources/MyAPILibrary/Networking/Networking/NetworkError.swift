import Foundation

public class NetworkError: Codable {
    
    let message: String
    let key: String?
    
}
