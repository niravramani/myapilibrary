import Foundation

// MARK: - Enums

public enum NetworkEnvironment {
    case dev
    case production
    case stage
}
