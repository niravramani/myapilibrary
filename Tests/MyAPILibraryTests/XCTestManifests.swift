import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(MyAPILibraryTests.allTests),
    ]
}
#endif
