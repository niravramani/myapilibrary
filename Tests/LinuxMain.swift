import XCTest

import MyAPILibraryTests

var tests = [XCTestCaseEntry]()
tests += MyAPILibraryTests.allTests()
XCTMain(tests)
